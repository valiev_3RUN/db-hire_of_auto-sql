
CREATE TABLE History_change
(
  id_log serial  PRIMARY KEY UNIQUE, --id лога
  id_user varchar(64) NOT NULL, --user который изменил
  operation varchar(64) NOT NULL, --операция, которую произвел user
  time_change timestamp NOT NULL default now(),	--время и дата когда user это сделал
  tab_name varchar(64) NOT NULL, --в какой таблице user это сделал
  primary_key bigInt, --id строки из измененной таблицы
  change_of_table varchar(64) --что изменил user

);
select id_user as "ID Пользователя", operation as "Операция", time_change as "Время изменения", tab_name as "Измененная таблица", primary_key as "ID строки", change_of_table as "Перечень изменений"   from History_change;

/*Триггерная функции добавления*/
CREATE OR REPLACE FUNCTION change_insert() RETURNS TRIGGER AS $$
DECLARE
	Prime_key bigInt;
	current_change varchar; 
BEGIN
	if(TG_OP = 'INSERT' ) THEN
           --Clients
                if(TG_TABLE_NAME = 'clients') THEN
                        Prime_key = NEW.id_client;
                        current_change = 'First_name: ' ||  NEW.first_name || ', Last_name: ' || NEW.last_name || ', address: ' || NEW.address || ', phone: ' || NEW.telephone || ', passport: ' || NEW.passport || ', driver_license: ' || NEW.driver_license;
                --Autos
                elseif(TG_TABLE_NAME = 'autos') THEN
                        Prime_key = NEW.id_auto;
                        current_change = 'Model: ' ||  NEW.Model || ', Mark: ' || NEW.Mark || ', Color: ' || NEW.Color;
                --Hire
                elseif(TG_TABLE_NAME = 'hire') THEN
                        Prime_key = NEW.id_hire;
                        current_change = 'id_auto: ' ||  NEW.id_auto || ', cost_auto: ' || NEW.cost_auto || ', cost_hire: ' || NEW.cost_hire || ', count_repairs: ' || NEW.count_repairs;
                --Contract
                elseif(TG_TABLE_NAME = 'contract') THEN
                        Prime_key = NEW.id_contract;
                        current_change = 'data_issue: ' ||  NEW.data_issue || ', data_return: ' || NEW.data_return || ', id_client: ' || NEW.id_client || ', id_hire: ' || NEW.id_hire;
                --Repairs
                elseif(TG_TABLE_NAME = 'repairs') THEN
                        Prime_key = NEW.id_repair;
                        current_change = 'id_hire: ' ||  NEW.id_hire || ', id_client: ' || NEW.id_client || ', damage: ' || NEW.damage || ', cost_repair: ' || NEW.cost_repair;
                end if;

        insert into History_change (id_user, operation, time_change, tab_name, primary_key, change_of_table) values (session_user, TG_OP, now(), TG_TABLE_NAME, prime_key, current_change);
	end if;
return null;
END;
$$ LANGUAGE plpgsql;


/*Триггеры, которые срабатывают при добавлении*/
CREATE TRIGGER trigger_insert
AFTER INSERT OR UPDATE OR DELETE ON Autos
    FOR EACH ROW EXECUTE PROCEDURE change_insert();

CREATE TRIGGER trigger_insert
AFTER INSERT OR UPDATE OR DELETE ON Hire
    FOR EACH ROW EXECUTE PROCEDURE change_insert();

CREATE TRIGGER trigger_insert
AFTER INSERT OR UPDATE OR DELETE ON Clients
    FOR EACH ROW EXECUTE PROCEDURE change_insert();

CREATE TRIGGER trigger_insert
AFTER INSERT OR UPDATE OR DELETE ON Repairs
    FOR EACH ROW EXECUTE PROCEDURE change_insert();

CREATE TRIGGER trigger_insert
AFTER INSERT OR UPDATE OR DELETE ON Contract 
    FOR EACH ROW EXECUTE PROCEDURE change_insert();
	

	
	
/*Триггерная функции удаления*/
CREATE OR REPLACE FUNCTION change_delete() RETURNS TRIGGER AS $$
DECLARE
	Prime_key bigInt;
	current_change varchar; 
BEGIN
	if(TG_OP = 'DELETE' ) THEN
           --Clients
                if(TG_TABLE_NAME = 'сlients') THEN
                        Prime_key = OLD.id_client;
                        current_change = 'First_name: ' ||  OLD.first_name || ', Last_name: ' || OLD.last_name || ', address: ' || OLD.address || ', phone: ' || OLD.telephone || ', passport: ' || OLD.passport || ', driver_license: ' || OLD.driver_license;
                --Autos
                elseif(TG_TABLE_NAME = 'autos') THEN
                        Prime_key = OLD.id_auto;
                        current_change = 'Model: ' ||  OLD.Model || ', Mark: ' || OLD.Mark || ', Color: ' || OLD.Color;
                --Hire
                elseif(TG_TABLE_NAME = 'hire') THEN
                        Prime_key = OLD.id_hire;
                        current_change = 'id_auto: ' ||  OLD.id_auto || ', cost_auto: ' || OLD.cost_auto || ', cost_hire: ' || OLD.cost_hire || ', count_repairs: ' || OLD.count_repairs;
                --Contract
                elseif(TG_TABLE_NAME = 'contract') THEN
                        Prime_key = OLD.id_contract;
                        current_change = 'data_issue: ' ||  OLD.data_issue || ', data_return: ' || OLD.data_return || ', id_client: ' || OLD.id_client || ', id_hire: ' || OLD.id_hire;
                --Repairs
                elseif(TG_TABLE_NAME = 'repairs') THEN
                        Prime_key = OLD.id_repair;
                        current_change = 'id_hire: ' ||  OLD.id_hire || ', id_client: ' || OLD.id_client || ', damage: ' || OLD.damage || ', cost_repair: ' || OLD.cost_repair;
                end if;

        insert into History_change (id_user, operation, time_change, tab_name, primary_key, change_of_table) values (session_user, TG_OP, now(), TG_TABLE_NAME, prime_key, current_change);
	end if;
return null;
END;
$$ LANGUAGE plpgsql;

/*Триггеры, срабатывающие при удалении*/
CREATE TRIGGER trigger_delete
AFTER INSERT OR UPDATE OR DELETE ON Autos
    FOR EACH ROW EXECUTE PROCEDURE change_delete();
	
CREATE TRIGGER trigger_delete
AFTER INSERT OR UPDATE OR DELETE ON Hire
    FOR EACH ROW EXECUTE PROCEDURE change_delete();

CREATE TRIGGER trigger_delete
AFTER INSERT OR UPDATE OR DELETE ON Clients
    FOR EACH ROW EXECUTE PROCEDURE change_delete();

CREATE TRIGGER trigger_delete
AFTER INSERT OR UPDATE OR DELETE ON Repairs
    FOR EACH ROW EXECUTE PROCEDURE change_delete();

CREATE TRIGGER trigger_delete
AFTER INSERT OR UPDATE OR DELETE ON Contract
    FOR EACH ROW EXECUTE PROCEDURE change_delete();
	
/*Триггерная функция обновления*/
CREATE OR REPLACE FUNCTION change_update() RETURNS TRIGGER AS $$
DECLARE
	Prime_key bigInt;
	current_change varchar; 
BEGIN
	if(TG_OP = 'UPDATE' ) THEN
           --Clients
                if(TG_TABLE_NAME = 'clients') THEN
                        Prime_key = OLD.id_client;
                        current_change = 'First_name: ' ||  OLD.first_name || ', Last_name: ' || OLD.last_name || ', address: ' || OLD.address || ', phone: ' || OLD.telephone || ', passport: ' || OLD.passport || ', driver_license: ' || OLD.driver_license;
                --Autos
                elseif(TG_TABLE_NAME = 'autos') THEN
                        Prime_key = OLD.id_auto;
                        current_change = 'Model: ' ||  OLD.Model || ', Mark: ' || OLD.Mark || ', Color: ' || OLD.Color;
                --Hire
                elseif(TG_TABLE_NAME = 'hire') THEN
                        Prime_key = OLD.id_hire;
                        current_change = 'id_auto: ' ||  OLD.id_auto || ', cost_auto: ' || OLD.cost_auto || ', cost_hire: ' || OLD.cost_hire || ', count_repairs: ' || OLD.count_repairs;
                --Contract
                elseif(TG_TABLE_NAME = 'contract') THEN
                        Prime_key = OLD.id_contract;
                        current_change = 'data_issue: ' ||  OLD.data_issue || ', data_return: ' || OLD.data_return || ', id_client: ' || OLD.id_client || ', id_hire: ' || OLD.id_hire;
                --Repairs
                elseif(TG_TABLE_NAME = 'repairs') THEN
                        Prime_key = OLD.id_repair;
                        current_change = 'id_hire: ' ||  OLD.id_hire || ', id_client: ' || OLD.id_client || ', damage: ' || OLD.damage || ', cost_repair: ' || OLD.cost_repair;
                end if;

        insert into History_change (id_user, operation, time_change, tab_name, primary_key, change_of_table) values (session_user, TG_OP, now(), TG_TABLE_NAME, prime_key, current_change);
	end if;
return null;
END;
$$ LANGUAGE plpgsql;


/*Триггеры, срабатывающие при обновлении*/
CREATE TRIGGER trigger_update
AFTER INSERT OR UPDATE OR DELETE ON Autos

    FOR EACH ROW EXECUTE PROCEDURE change_update();
CREATE TRIGGER trigger_update
AFTER INSERT OR UPDATE OR DELETE ON Hire
    FOR EACH ROW EXECUTE PROCEDURE change_update();

CREATE TRIGGER trigger_update
AFTER INSERT OR UPDATE OR DELETE ON Clients
    FOR EACH ROW EXECUTE PROCEDURE change_update();

CREATE TRIGGER trigger_update
AFTER INSERT OR UPDATE OR DELETE ON Repairs
    FOR EACH ROW EXECUTE PROCEDURE change_update();

CREATE TRIGGER trigger_update
AFTER INSERT OR UPDATE OR DELETE ON Contract
    FOR EACH ROW EXECUTE PROCEDURE change_update();
