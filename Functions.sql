/*Добавление клиента*/
-- select add_client('Абдулла', 'Габидуллин', 'Новая басманная', '89851215011', '111213', '111213');
-- select add_client('Водем', 'Горув', 'Новая басманная', '89851215011', '111214', '111214');
-- select add_client('Траф', 'Трафимов', 'Новая басманная', '89851215011', '111216', '111215');
-- select add_client('НеТраф', 'НеТрафимов', 'Новая басманная', '89851215011', '111210', '111210');
CREATE FUNCTION Add_client(f_name varchar(20), l_name varchar (20),addressP varchar(25),
phone char(11) , passportP char (10), prava char (10)) returns int
AS $$
	DECLARE id int;
	BEGIN
		INSERT INTO Clients (first_name, last_name, address, telephone, passport, driver_license) 
			VALUES (f_name, l_name, addressP, phone, passportP, prava)
			returning id_client into id;
			return id;
	END;
$$ LANGUAGE plpgsql security definer;



/*Редактирование клиента по номеру*/
CREATE FUNCTION Edit_Client (id INT, f_name varchar(20), l_name varchar (20),
addressP varchar(25),phone char(11) , passportP char (10), prava char (10)) RETURNS INT
AS $$
	BEGIN
		UPDATE Clients SET first_name = f_name, last_name = l_name, address = addressP,
		telephone = phone, passport = passportP, driver_license = prava
		where id_client  = id;
		return 0;
	END;
$$ LANGUAGE plpgsql security definer;

/*Удаление клиента по номеру*/
CREATE FUNCTION Delete_client (id INT) RETURNS INT
AS $$
	BEGIN
	if EXISTS (SELECT id_client from Clients where id_client = id) then
		DELETE FROM Clients WHERE id_client = id;
		else  raise exception  'Клиент не найден';
		end if;
		return 0;
	END;
$$ LANGUAGE plpgsql security definer;





/*Просто добаить контракт зарегистрированного клиента*/
-- select add_contract (1,'111213','2000-10-10', '2001-10-10');
-- select add_contract (2,'111214','2000-10-10', '2001-10-10');
CREATE FUNCTION Add_contract(id_hireP int, passportP varchar(9), data_issueP timestamp, data_returnP timestamp) returns int
AS $$
	DECLARE  id int;
	DECLARE new_id_contract int;
		BEGIN
			if EXISTS (select id_client, id_hire from clients, Hire where passport = passportP and id_hire = id_hireP) then
				SELECT id_client FROM clients WHERE passport = passportP into id;
				INSERT INTO Contract (data_issue, data_return, id_client, id_hire) VALUES (data_issueP, data_returnP, id, id_hireP)
				returning id_contract into new_id_contract;
				return new_id_contract;
			else raise exception 'Код проката или пасспортные данные клиента не определены';
			end if;
			return 0;				
		END
$$ LANGUAGE plpgsql security definer;  
 



 /*Зарегать клиента и добавить для него контракт*/
 --select Add_client_and_contract(1, 'Мух', 'Габи', 'Новая басманная', '79851215076', '123321', '123321', '2010-10-10', '2011-10-10');
 --select Add_client_and_contract(2, 'Абдулла', 'Габи', 'Новая басманная', '79851215076', '123322', '123322', '2010-10-10', '2011-10-10');
 --select Add_client_and_contract(3, 'Водем', 'Горув', 'Москва', '79851215076', '123323', '123323', '2010-10-10', '2011-10-10');
CREATE FUNCTION Add_client_and_contract(id_hireP int, f_name varchar(20), l_name varchar(20), addressP varchar(25), phone varchar(20), passportP varchar(10),
prava varchar(10), data_issueP timestamp, data_returnP timestamp) returns int
AS $$
	DECLARE id INT;
	BEGIN
		if EXISTS (SELECT id_hire from Hire where id_hire = id_hireP) then
				INSERT INTO Clients (first_name, last_name, address, telephone, passport, driver_license) VALUES
					(f_name, l_name, addressP, phone, passportP, prava)
				returning id_client into id;		
				INSERT INTO Contract (data_issue, data_return, id_client, id_hire)	VALUES
					(data_issueP, data_returnP, id, id_hireP)
				returning id_contract into id;
				return id;				
		else raise exception 'Код проката не определен'; return 0;
		end if;
	END
$$ LANGUAGE plpgsql security definer;
 


/*Регистрация автомобиля на прокат*/
--select add_hire(1, 150000, 200, 1);
--select add_hire(2, 200000, 2000, 0);
--select add_hire(3, 210000, 2500, 0);
--select add_hire(4, 240000, 3000, 0);
CREATE FUNCTION Add_hire(id_autoP int, cost_autoP int, cost_hireP int, count_repairsP int) returns int
AS $$
	DECLARE id int;
	BEGIN
	if exists (SELECT id_auto FROM Autos where id_auto = id_autoP) then
		INSERT INTO Hire (id_auto, cost_auto, cost_hire, count_repairs) VALUES (id_autoP, cost_autoP, cost_hireP, count_repairsP)
		returning id_hire into id; return id;
		else raise exception 'ID авто не найден';
		end if;
	END;
$$ LANGUAGE plpgsql security definer;


/*Редактирование  проката по номеру*/
CREATE FUNCTION Edit_hire(id int, cost1 int, cost2 int, count_repairsP int) returns int
AS $$
	BEGIN
		if exists (SELECT id_hire FROM Hire WHERE id_hire = id) then 
		UPDATE Hire SET cost_hire = cost1, cost_auto = cost2, count_repairs = count_repairsP WHERE id_hire = id;
		return id;
		else raise exception 'id hire not found';
		end if;
		
	END;
$$ LANGUAGE plpgsql security definer;

 select model from Autos where not exists (select model from autos where model = 'Меган')  and exists (select model from autos where model = 'm');



 
 /*Добавление автомобиля*/
--select add_auto('Поло', 'фольксваген', 'Белый');
--select add_auto('E36', 'BMW', 'Черный');
--select add_auto('Duster', 'Renault', 'Прозрачный');
--select add_auto('M5', 'BMW', 'Черный');
--select add_auto('m', '123','123');
CREATE FUNCTION Add_auto(modelP varchar(15), markP varchar(15), colorP varchar(10)) returns int
AS $$
	DECLARE id int;
	BEGIN
		INSERT INTO Autos (Model, Mark, Color) VALUES (modelP, markP, colorP)
		returning id_auto into id;
		return id;
	END;
$$ LANGUAGE plpgsql security definer;

/*Удаление автомобиля по номеру*/
CREATE FUNCTION delete_auto (id int) returns int
AS $$
	BEGIN		
	if exists (SELECT id_auto FROM Autos where id_auto = id) then
		DELETE FROM Autos WHERE id_auto = id;
		else raise exception 'ID авто не найден';
		end if;	
	RETURN 0;
	END;
$$ LANGUAGE plpgsql security definer;


/*Редактирование информации об автомобиля*/
CREATE FUNCTION Edit_auto(id_autoP int, modelP varchar(15), markP varchar(15), colorP varchar(10)) returns int
AS $$
	BEGIN
		if exists (SELECT id_auto from Autos where id_auto = id_autoP) then
			UPDATE Autos SET Model = modelP, Mark = markP, Color = colorP where id_auto = id_autoP; return id_autoP;
			else raise exception 'ID авто не найден'; return 0;
			end if;
	END;
$$ LANGUAGE plpgsql security definer;	
	

		
	
	
/*Добавление информации о ремонте*/
--SELECT add_repair(1, '123321', 'сломал', 1500);
CREATE FUNCTION Add_repair(id_hireP int, passportP varchar(9), damageP varchar(30), cost int) returns int
AS $$
	DECLARE id int;
	DECLARE new_id_repair int;
	BEGIN
		if exists (SELECT id_hire, passport from Hire, Clients where id_hire = id_hireP and passport = passportP ) then
		SELECT id_client from Clients where passport = passportP into id;
		INSERT INTO Repairs (id_hire, id_client, damage, cost_repair)
			VALUES (id_hireP, id, damageP, cost)
			returning id_repair into new_id_repair;
			return new_id_repair;
		else raise exception 'ID проката или пасспортные данные клиента не определены';
		end if;
	END;
$$ LANGUAGE plpgsql security definer;

SELECT  mark, cost_auto from Hire INNER JOIN Autos on hire.id_auto = autos.id_auto;



/*Вывод клиента	 */
CREATE  FUNCTION List_client() returns table
(ID_клиента int, "Имя" varchar(15) , "Фамилия" varchar(15), "Адрес" varchar(30), "Телефон" varchar(11), "Паспорт" varchar(9), "ВУ" varchar(11))
AS $$
BEGIN
RETURN QUERY
		SELECT  id_client,
				first_name, 
				last_name,
				address,
				telephone ,
				passport,
				driver_license	
		FROM Clients;
END;
$$ LANGUAGE plpgsql security definer;






/*Вывод автомобилей */
CREATE FUNCTION List_Autos() returns
TABLE (КодАвто int, "Модель" varchar(20), "Марка" varchar(20), "Цвет" varchar(10))
AS $$
	BEGIN
	return QUERY
	SELECT
			id_auto,
			Model,
			Mark,
			Color
	FROM Autos;		
	END
$$ LANGUAGE plpgsql security definer; 

/*Вывод информации о прокатах автомобилей */
CREATE FUNCTION List_hire() returns
TABLE (КодПроката int,Пасспорт varchar(9), "Марка" varchar(15), "Модель" varchar(15), ЦенаАвто int, ЦенаАренды int, КоличествоРемонтов int)
AS $$
	BEGIN
	return QUERY
		SELECT  Hire.id_hire,
				Clients.passport,
				mark,
				model,			
				cost_auto,
				cost_hire,
				count_repairs
		FROM Hire INNER JOIN Autos on Hire.id_auto = Autos.id_auto
				  INNER JOIN Contract on Hire.id_hire = Contract.id_hire
				  INNER JOIN Clients on Contract.id_client = Clients.id_client;
	END
$$ LANGUAGE plpgsql security definer;


/*Просмотр информации о ремонте*/
CREATE FUNCTION List_repairs() returns TABLE
(КодПроката int, "Фамилия" varchar(15), "Имя" varchar(15), "Марка" varchar(10),   "Повреждения" varchar(30), "Цена ремонта" int)
AS $$
	BEGIN	
	return QUERY
	SELECT	
			Repairs.id_hire,
			first_name,
			last_name,
			Autos.mark,
			damage,		   		    		
		    cost_repair
	FROM Repairs
	INNER JOIN Hire on Hire.id_hire = Repairs.id_hire
	INNER JOIN Autos on Autos.id_auto = Hire.id_auto
	INNER JOIN Clients on Clients.id_client = Repairs.id_client;
	
	END
$$ LANGUAGE plpgsql security definer;

/*Вывод договора */
CREATE FUNCTION List_contract() returns
table (КодКонтракта int,"Фамилия" varchar(15), "Имя" varchar(15), "Паспорт" varchar(9), "Марка" varchar(10), ЦенаАренды int, ДатаАренды timestamp, Возврат timestamp )
AS $$
	BEGIN
	 return QUERY
		SELECT id_contract,
			   first_name,
			   last_name,
			   passport,
			   Autos.mark,
			   Hire.cost_hire,
			   data_issue,
			   data_return
		FROM Contract
		INNER JOIN Clients on Clients.id_client = Contract.id_client
		INNER JOIN Hire on Hire.id_hire = Contract.id_hire
		INNER JOIN Autos on Autos.id_auto = Hire.id_auto;
	END;
	
$$ LANGUAGE plpgsql security definer; 



/*Лишение всех доступа к функциям*/
revoke all  on ALL FUNCTIONS IN SCHEMA public from public;
/*revoke all  on function add_client from  group employee  PUBLIC;
revoke all on function Add_client_and_contract from PUBLIC;
revoke all on function Add_contract from PUBLIC;
revoke all on function Add_hire from PUBLIC;
revoke all on function Add_repair from PUBLIC;
revoke all on function Add_auto from PUBLIC;
revoke all on function Create_user from PUBLIC;

revoke all on function Delete_auto from PUBLIC;
revoke all on function Delete_client from PUBLIC;
revoke all on function Delete_user from PUBLIC;

revoke all on function Edit_auto from PUBLIC;
revoke all on function Edit_client from PUBLIC;
revoke all on function Edit_hire from PUBLIC;
revoke all on function Edit_user from PUBLIC;
revoke all on function Edit_repairs from PUBLIC;

revoke all on function List_client from PUBLIC;
revoke all on function List_contract from PUBLIC;
revoke all on function List_repairs from PUBLIC;
revoke all on function List_hire from PUBLIC;
revoke all on function List_Autos from PUBLIC;*/