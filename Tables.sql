
/*Создание таблиц*/
CREATE TABLE  Autos (
	--id_auto INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	id_auto  SERIAL PRIMARY KEY NOT NULL,
	Model VARCHAR(15) CHECK(Model != ''),
	Mark VARCHAR(15) CHECK(Mark != ''),
	Color VARCHAR(10) CHECK(Color != '')
);
CREATE TABLE Hire (
	--id_hire INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	id_hire SERIAL PRIMARY KEY NOT NULL,
	id_auto INT REFERENCES Autos(id_auto) ON DELETE  CASCADE ON UPDATE CASCADE CHECK(id_auto > 0) NOT NULL ,
	cost_auto INT CHECK(cost_auto > 0),
	cost_hire INT CHECK(cost_hire > 0),
	count_repairs INT CHECK(count_repairs >= 0) default 0
);

CREATE TABLE Clients (
	--id_client INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	id_client SERIAL PRIMARY KEY NOT NULL,
	first_name VARCHAR(20) CHECK(first_name != ''),
	last_name VARCHAR(20) CHECK(last_name != ''),
	address VARCHAR(25) CHECK(address != ''),
	telephone varchar (11) NOT NULL,
	passport varchar (9)  NOT NULL UNIQUE, 
	driver_license varchar (11) NOT NULL UNIQUE
);

CREATE TABLE Repairs (
	--id_repair INT PRIMARY KEY  NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	id_repair SERIAL PRIMARY KEY NOT NULL,
	id_hire INT REFERENCES Hire(id_hire)  ON DELETE CASCADE ON UPDATE CASCADE NOT NULL ,
	id_client  INT REFERENCES Clients(id_client)  ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
	damage VARCHAR(30) CHECK (damage != ''),
	cost_repair INT CHECK (cost_repair > 0)
);


CREATE TABLE Contract  (
	--id_contract INT PRIMARY KEY NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1 INCREMENT BY 1),
	id_contract SERIAL PRIMARY KEY NOT NULL,
	data_issue timestamp,
	data_return timestamp CHECK(data_return > data_issue),
	id_client INT REFERENCES Clients(id_client) ON DELETE CASCADE ON UPDATE CASCADE CHECK(id_client > 0) NOT NULL,
	id_hire  INT REFERENCES Hire(id_hire) ON DELETE CASCADE ON UPDATE CASCADE  CHECK(id_hire > 0) NOT NULL
	
);

--Create user Pitushara with password '1234' in role Employee;
--=# GRANT SELECT, UPDATE, INSERT ON ALL TABLES IN SCHEMA public TO "dmosk";