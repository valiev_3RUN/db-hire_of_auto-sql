
/*Создание ролей*/

/*Лишение всех доступа к функциям*/
revoke all  on ALL FUNCTIONS IN SCHEMA public from public;


CREATE ROLE administrator LOGIN CREATEROLE;
	create user valiev with password 'valiev' in role administrator;

CREATE ROLE employee login;
	grant usage on schema public to employee;
	grant execute on function list_hire() to employee;
	grant execute on function Public.Edit_hire(id int, cost1 int, cost2 int, count_repairsP int) to employee;
	grant execute on function Public.Add_hire(id_autoP int, cost_autoP int, cost_hireP int, count_repairsP int)  to employee;
	grant execute on function Public.list_autos() to employee;
	grant execute on function Public.delete_auto(id int) to employee;
	grant execute on function Public.Edit_auto(id_autoP int, modelP varchar(15), markP varchar(15), colorP varchar(10)) to employee;
	grant execute on function Public.Add_auto(modelP varchar(15), markP varchar(15), colorP varchar(10)) to employee;
	grant execute on function Public.list_repairs() to employee;
	grant execute on function Public.Add_repair(id_hireP int, passportP varchar(9), damageP varchar(30), cost int) to employee;
	
CREATE ROLE registrar login;
	--grant connect on database postgres to registrar;
	grant usage on schema public to registrar;
	
	grant execute on function public.list_client() to registrar;
	grant execute on function Public.Add_client(f_name varchar(20), l_name varchar (20),addressP varchar(25),
									     phone char(11) , passportP char (10), prava char (10)) to registrar;
	grant execute on function Public.Edit_Client (id INT, f_name varchar(20), l_name varchar (20),
					addressP varchar(25),phone char(11) , passportP char (10), prava char (10)) to registrar;
	grant execute on function Public.Delete_client (id INT) to registrar;
	grant execute on function Public.list_client() to registrar;
	grant execute on function Public.Add_client_and_contract(id_hireP int, f_name varchar(20), l_name varchar(20), addressP varchar(25), phone varchar(20), passportP varchar(10),
							  prava varchar(10), data_issueP timestamp, data_returnP timestamp) to registrar;

	grant execute on function Public.Add_contract(id_hireP int, passportP varchar(9), data_issueP timestamp, data_returnP timestamp) to registrar;
	grant execute on function Public.list_contract() to registrar;

	
	
/*Создание юзера*/
--select create_user('купш, '1234', 'registrar');
CREATE FUNCTION Create_user(name varchar(30), password varchar(30), name_role varchar(30)) returns int
AS $$
	BEGIN
		execute 'Create user ' || name || ' with password ''' || password ||''' in role ' || name_role ||';';
		return 0;
	END;
$$ LANGUAGE plpgsql;


/*Изменение юзера*/
--select edit_user('registrar', 'employee', 'valiev');
CREATE FUNCTION Edit_user(name_role varchar(30), new_role varchar(30), user_name varchar(30)) returns int
AS $$
	BEGIN
		execute 'alter group ' || name_role || ' drop user ' || user_name || ' ; alter group ' || new_role || ' add user ' || user_name || ';';
		return 0;
	END;
$$ LANGUAGE plpgsql;




/*Удаление юзера*/
CREATE FUNCTION Delete_user(user_name text) RETURNS int --
AS $$
	begin
	execute 'drop user ' || user_name || ';';
		return 0;
	end;
$$ LANGUAGE plpgsql;
