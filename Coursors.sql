create table compressed_logs(
	id_log serial primary key,
	operation varchar(30) not null,	
	tab_name varchar(30) not null,
	primary_key varchar(30)not null,
	change_of_table varchar(300)
);



create or replace function compress_logs() returns integer
as $$
declare cur cursor for select operation, tab_name, primary_key, change_of_table from History_change order by id_log;
	declare ac text;
			declare acTable text;
			declare acId text;
			declare chData text;
			declare tableData record;
begin
	open cur;
	ac:= '';
	fetch cur into tableData;
	ac := tableData.operation;
	acTable := tableData.tab_name;
	chData := tableData.change_of_table;	
	acId := tableData.primary_key;	
	loop
		fetch cur into tableData;
		exit when not found;
		/*if ac = '' then
			ac := tableData.operation;
			acTable := tableData.tab_name;
			chData := tableData.change_of_table;	
			acId := tableData.primary_key;	
		else */
			if  ac = tableData.operation and acTable = tableData.tab_name then
				chData := chData || ' || ' || tableData.change_of_table;
				acId := acId || ', ' || tableData.primary_key;	
			else
				insert into compressed_logs(operation, tab_name, primary_key, change_of_table) values (ac, acTable, acId, chData);
				ac := tableData.operation;
				acTable := tableData.tab_name;
				chData := tableData.change_of_table;
				acId := tableData.primary_key;	
			end if;
		--end if;
	end loop;
	if ac <> '' then
		insert into compressed_logs(operation, tab_name, primary_key, change_of_table) values (ac, acTable, acId, chData);
	end if;
	close cur;
	return 0;
end;
$$
language plpgsql;

	